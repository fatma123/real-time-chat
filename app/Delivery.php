<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{

    protected $fillable = ['user_id', 'identity_image', 'personal_image','license','car_image','car_type','car_model','car_form_image','car_license'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function carts()
    {
        return $this->belongsTo(Cart::class,'delivery_id');
    }

    protected $guarded = ['id'];

}
