<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function getValueAttribute()
    {
        return getLang($this,'value');
    }

    public function getTitleAttribute()
    {
        return getLang($this,'title');
    }
}
