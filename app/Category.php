<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];

    public function institutions()
    {
        return $this->hasMany(Institution::class,'category_id');
    }

    public function menu()
    {
        return Menu::whereInstitutionId(null);
    }

    public function getMenuAttribute()
    {
        return Menu::whereInstitutionId(null)->get();
    }

    public function getNameAttribute()
    {
        return getLang($this,'name');
    }
}
