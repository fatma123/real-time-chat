<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $fillable = ['lat','lng','address','addressable_id','addressable_type','description'];

    public function addressable()
    {
        return $this->morphTo();
    }
}
