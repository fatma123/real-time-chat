<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = ['id'];

    public function getFullImageAttribute()
    {
        return getimg($this->image);
    }

    public  function institution(){

        return $this->belongsTo(Institution::class,'institution_id');
    }
}
