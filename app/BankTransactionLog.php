<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTransactionLog extends Model
{
    //
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(user::class,'delivery_id');
    }

}
