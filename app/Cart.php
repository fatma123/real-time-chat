<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->site_percentage = setting('webPercentage') != null ? setting('webPercentage') : 0;
        });
    }

    public function items()
    {
        return $this->hasMany(CartItem::class,'cart_id');
    }

    public function address()
    {
        return $this->morphOne(Address::class,'addressable');
    }

    public function delivery()
    {
        return $this->belongsTo(User::class,'delivery_id');
    }

    public function images()
    {
        return $this->hasMany(CartImages::class,'cart_id');
    }

    public function feesLogs()
    {
        return $this->hasMany(CartFeesLog::class,'cart_id');
    }

    public function institution()
    {
        return $this->belongsTo(Institution::class,'institution_id');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class,'rateable');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'cart_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }


}
