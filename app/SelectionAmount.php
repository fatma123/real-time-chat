<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectionAmount extends Model
{
    protected $fillable = ['selection_id','amount'];

    public function selections()
    {
        return $this->belongsTo(DeliverySelection::class,'selection_id');
    }
}
