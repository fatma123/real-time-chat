<?php


namespace App\Http\Traits;

use App\Schedule;


trait ScheduleOperation
{
   public function RegisterSchedule($request)
  {
      $inputs = $request->all();
    
      return Schedule::create($inputs);
  }

    public function UpdateSchedule($Schedule, $request)
    {
        $inputs = $request->except('image');
        return $Schedule->update($inputs);
    }


}