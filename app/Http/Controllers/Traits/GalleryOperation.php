<?php


namespace App\Http\Traits;


use App\Address;
use App\Gallery;
use App\User;
use Hash;
use Illuminate\Http\Request;

trait GalleryOperation
{
   public function RegisterGallery($request)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      return Gallery::create($inputs);
  }

    public function UpdateGallery($Gallery, $request)
    {
        $inputs = $request->except('image');
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $Gallery->update(['image' => $picture]);
            }
        }
        return $Gallery->update($inputs);
    }


}