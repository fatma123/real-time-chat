<?php


namespace App\Http\Traits;

use App\Cart;
use App\Category;
use Illuminate\Http\Request;

trait CartOperations
{
    public function StoreDetails(Request $request)
    {

        $inputs = $request->all();
        $cart =Cart::create($inputs);
        return $cart;
    }

}