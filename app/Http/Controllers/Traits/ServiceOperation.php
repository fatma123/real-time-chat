<?php


namespace App\Http\Traits;


use App\Address;
use App\Service;
use App\User;
use Hash;
use Illuminate\Http\Request;

trait ServiceOperation
{
   public function RegisterService($request)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      return Service::create($inputs);
  }

    public function UpdateService($Service, $request)
    {
        $inputs = $request->except('image');
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $Service->update(['image' => $picture]);
            }
        }
        return $Service->update($inputs);
    }


}