<?php


namespace App\Http\Traits;
use App\User;
use App\Delivery;
use Hash;

trait UserOperation
{
   public function RegisterUser($request, $role)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      $inputs['password']=Hash::make($request->password);
      $inputs['role'] = $role;

      $inputs['verification_code']=12345;
      $inputs['resend_verification_code']=now();

      if ($request->has('identity_image')) {
          $inputs['identity_image'] = uploader($request, 'identity_image');
      }
      if ($request->has('personal_image')) {
          $inputs['personal_image'] = uploader($request, 'personal_image');
      }
      if ($request->has('car_image')) {
          $inputs['car_image'] = uploader($request, 'car_image');
      }
      if ($request->has('car_form_image')) {
          $inputs['car_form_image'] = uploader($request, 'car_form_image');
      }


      $user=User::create($inputs);

      if ($request->has('address'))
          $user->address()->create($request->only('lat', 'lng', 'address'));

      $inputs['user_id']=$user->id;
      if ($role=='delivery'){
          Delivery::create($inputs);
      }
      return $user;
  }

}