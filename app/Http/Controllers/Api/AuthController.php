<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;
use App\Http\Traits\UserOperation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use JWTAuth;

class AuthController extends Controller
{
use ApiResponses,UserOperation;

//    public function __construct()
//    {
////        dd('hi');
//        $this->middleware('auth:api', ['except' => ['login','register','refresh','unique_mail_phone']]);
//    }

    public function Register(Request $request,$role){

//        dd('hi');
        $request['role']=$role;

        if ($role == 'customer') {
            $rules = [
                'name' => 'sometimes|max:191',
                'phone' => 'required|numeric|unique:users,phone',
                'email' => 'sometimes|unique:users,email',
                'password' => 'required|string|max:191',
                'image'=>'required|image',
                'lat' => 'required',
                'lng' => 'required',
                'address' => 'required',

            ];

            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
        }


        if ($role =='delivery'){
            $rules=[
                'name' => 'sometimes|max:191',
                'phone' => 'required|numeric|unique:users,phone',
                'email' => 'sometimes|unique:users,email',
                'password' => 'required|string|max:191',
                'image'=>'required|image',
                'identity_image' => 'required|image|max:2048',
                'personal_image' => 'required|image|max:2048',
                'license' => 'required|max:191',
                'car_image' => 'required|image|max:2048',
                'car_type' => 'required|max:191',
                'car_model' => 'required|max:191',
                'car_form_image' => 'required|image|max:2048',
                'car_license' => 'required|max:191',

            ];
            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
        }
        {$user =$this->RegisterUser($request,$role);

        }
        $user=User::find($user->id);

        $token = JWTAuth::fromUser($user);
        $user['token'] = $token;
        $user =  new UserResource($user);
        if ($token && $user) {return $this->createdResponse($user);}
        $this->unKnowError();

    }

    Public function Login(Request $request,$role){

        $request['role']=$role;
        if($role == 'customer'){
            $rules = [
                'phone'=>'required|exists:users,phone',
                'fcm_android_token'=>'required_without:fcm_ios_token',
                'fcm_ios_token'=>'required_without:fcm_android_token',
            ];
            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
            $credentials = request(['phone', 'password']);
            if (! $token = auth('api')->attempt($credentials)) {
                return $this->apiResponse(null, 'عفوا هناك خطا في رقم الهاتف', 400);
            }
        }

        if($role == 'delivery'){
            $rules = [
                'phone'=>'required|exists:users,phone',
                'password' => 'required|string|max:191',
                'fcm_android_token'=>'required_without:fcm_ios_token',
                'fcm_ios_token'=>'required_without:fcm_android_token',
            ];
            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
            $credentials = request(['phone', 'password']);
            if (! $token = auth('api')->attempt($credentials)) {
                return $this->apiResponse(null, 'عفوا هناك خطا في كلمة المرور او رقم الهاتف', 400);
            }
        }

        $user = api()->user();
        if ($request->has('fcm_token_android')) {
            $user->fcm_token_android = $request['fcm_token_android'];
        }
        if ($request->has('fcm_token_ios')) {
            $user->fcm_token_ios = $request['fcm_token_ios'];
        }
        $user->save();

        return $this->respondWithToken($token);

    }

    protected function respondWithToken($token)
    {
        $user=api()->user();

        $user['token']=$token;

        return $this->apiResponse(new UserResource($user));
    }

    public function Logout(){
        auth('api')->logout();
        return $this->apiResponse(['message' => 'Successfully logged out']);
    }

    public function profile(){
        $user=api()->user();
        $user['token']= JWTAuth::getToken()->get();
        return $this->apiResponse( new UserResource( auth('api')->user()));
    }

    public function EditProfile(Request $request, $role){

        $request['role']=$role;

        if ($role == 'customer') {
            $rules = [
                'name' => 'sometimes|max:191',
                'phone' => 'required|numeric|unique:users,phone',
                'email' => 'sometimes|unique:users,email',
                'password' => 'required|string|max:191',
                'image'=>'required|image',
                'lat' => 'required',
                'lng' => 'required',
                'address' => 'required',

            ];

            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
        }


        if ($role =='delivery'){
            $rules=[
                'name' => 'sometimes|max:191',
                'phone' => 'required|numeric|unique:users,phone',
                'email' => 'sometimes|unique:users,email',
                'password' => 'required|string|max:191',
                'image'=>'required|image',
                'identity_image' => 'required|image|max:2048',
                'personal_image' => 'required|image|max:2048',
                'license' => 'required|max:191',
                'car_image' => 'required|image|max:2048',
                'car_type' => 'required|max:191',
                'car_model' => 'required|max:191',
                'car_form_image' => 'required|image|max:2048',
                'car_license' => 'required|max:191',

            ];
            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}
        }
    }





}
