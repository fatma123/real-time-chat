<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'is_admin'=>$this->is_active,
            'image'=>getimg($this->image),
            'delivery' => $this->deliveryDetails()->exists() ? new DeliverySingleResource($this->deliveryDetails) : new stdClass(),
            'lat' => optional($this->address()->first())->lat ?? 0,
            'lng' => optional($this->address()->first())->lng ?? 0,
            'address' => optional($this->address()->first())->address,
            'token'=>$this->token,

        ];
    }
}
