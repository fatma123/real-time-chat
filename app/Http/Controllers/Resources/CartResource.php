<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'client_name'=>$this->client_name,
            'client_phone'=>$this->client_phone,
            'notes'=>$this->notes,
             'status'=>$this->status
        ];
    }
}
