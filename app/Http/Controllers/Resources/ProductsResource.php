<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ar_name' => $this->ar_name,
            'en_name' => $this->en_name,
            'ar_description' => $this->ar_description,
            'en_description' => $this->en_description,
            'price' => $this->price,
            'is_active' => $this->is_active,
            'sub_category_id' => $this->sub_category_id,
            'discount' => $this->discount,
            'expired_date' => $this->expired_date,
            'image' => $this->image,
            'quantity' => $this->quantity,
            'rate'=>$this->rate(),
            'gallery' => $this->gallery == array() ? null : $this->gallery->transform(function ($q) {
                return ['id' => $q->id, 'image' => getimg($q->image)
                ];
            }),



        ];
    }
}
