<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class inboxCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($q){
                $admin=User::where('role','admin')->first();
                $data=[
                    'order_id'=>$q->order_id,
                    'message'=>$q->message,
                    'last_update'=>$q->updated_at->format('Y-m-d H:i'),
                    'is_closed'=>$q->is_closed,
                    'admin_id'=>$admin->id,
                    'admin_name'=>$admin->name,
                    'admin_image'=>getimg($admin->imae)

                ];
                if ($q->flag=='user'){
                    $data['user_id']=$q->order->user->id;
                    $data['user_image']=getimg($q->order->user->image);
                    $data['user_name']=$q->order->user->name;
                    $data['user_role']=$q->order->user->role;
                }else{
                    $data['user_id']=$q->order->refers->first()->id;
                    $data['user_image']=getimg($q->order->refers->first()->image);
                    $data['user_name']=$q->order->refers->first()->name;
                    $data['user_role']=$q->order->refers->first()->role;
                }
                return $data;
            }),
            'pagination' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ],
        ];
    }

    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        unset($originalContent['links'],$originalContent['meta']);
        $response->setData($originalContent);
    }
}
