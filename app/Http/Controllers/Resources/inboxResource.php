<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class inboxResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $admin=User::where('role','admin')->first();
        $data=[
            'order_id' => (integer)$this->order_id,
            'message'=>$this->message,
            'last_update'=>$this->updated_at->format('Y-m-d H:i'),
            'is_closed'=>$this->is_closed,
            'admin_id'=>$admin->id,
            'admin_name'=>$admin->name,
            'admin_image'=>getimg($admin->imae)

        ];
        info($this->flag);
        if ($this->flag=='user'){
            $data['user_id']=$this->order->user->id;
            $data['user_image']=getimg($this->order->user->image);
            $data['user_name']=$this->order->user->name;
            $data['user_role']=$this->order->user->role;
        }else{
            $data['user_id']=$this->order->refers->first()->id;
            $data['user_image']=getimg($this->order->refers->first()->image);
            $data['user_name']=$this->order->refers->first()->name;
            $data['user_role']=$this->order->refers->first()->role;
        }

        return $data;



    }
}
