<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliverySingleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        dd('hi');
        return [
            'id'=>$this->id,
            'identity_image'=>getimg($this->identity_image),
            'personal_image'=>getimg($this->personal_image),
            'car_image'=>getimg($this->car_image),
            'car_form_image'=>getimg($this->car_form_image),
            'license'=>$this->license,
            'car_type'=>$this->car_type,
            'car_model'=>$this->car_model,
            'car_license'=>$this->car_license,
        ];
    }
}
