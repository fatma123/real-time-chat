<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'lawyers'=>$this->collection->transform(function ($q){
                return [
                    'id'=>$q->id,
                    'name'=>$q->name,
                    'email'=>$q->email,
                    'phone'=>$q->phone,
                    'birth_date'=>$q->birth_date,
                    'gender'=>$q->gender,
                    'role'=>$q->role,
                    'is_active'=>$q->is_active,
                    'socials'=>json_decode($q->socials),
                    'city'=>$q->city_id==null?0:$q->city->name,
                    'city_id'=>$q->city_id==null?0:$q->city->id,
                    'country'=>$q->city_id==null?0:$q->city->country->name,
                    'country_id'=>$q->city_id==null?0:$q->city->country->id,
                    'fcm_token_android'=>$q->fcm_token_android,
                    'fcm_token_ios'=>$q->fcm_token_ios,
                    'image'=>getimg($q->image),
                    'token'=>$q->token,
                    'lawyer'=>$q->lawyer==null?null:new cartItems($q->lawyer),
                    'services'=>$q->services==array()?null: serviceResource::collection($q->services),
                    'Certifications'=>$q->certifications==array()?null: $q->certifications->transform(function ($q){
                        return ['id'=>$q->id,'certification'=>getimg($q->file)];
                    })
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]

        ];
    }

    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        unset($originalContent['links'],$originalContent['meta']);
        $response->setData($originalContent);
    }
}
