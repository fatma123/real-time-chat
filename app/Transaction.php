<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];
    // payed raw => where type eq cash main payed is amount which payed from delivery to site ..



    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
