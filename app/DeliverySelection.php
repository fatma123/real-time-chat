<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliverySelection extends Model
{
    protected $guarded = ['id'];

    public function getNameAttribute()
    {
        return getLang($this,'name');
    }

    public function amounts()
    {
        return $this->hasMany(SelectionAmount::class,'selection_id');
    }
}
