<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartFeesLog extends Model
{
    protected $guarded = ['id'];
    public function delivery()
    {
        return $this->belongsTo(User::class,'delivery_id');
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class,'cart_id');
    }
}
