<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $guarded = ['id'];
    public function institution()
    {
        return $this->belongsTo(Institution::class,'institution_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
