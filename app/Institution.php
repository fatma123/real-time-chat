<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $fillable = ['ar_name','en_name','image','ar_description','en_description','available','category_id'];

    public function address()
    {
        return $this->morphOne(Address::class,'addressable');
    }

    public function menu()
    {
        return $this->hasMany(Menu::class,'institution_id');
    }


    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class,'institution_id');
    }


    public function workTimes()
    {
        return $this->hasMany(InstitutionWorkTime::class,'institution_id');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class,'rateable');
    }

    public function getDescriptionAttribute()
    {
        return getLang($this,'description');
    }

    public function getNameAttribute()
    {
        return getLang($this,'name');
    }
}
