<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $guarded = ['id'];

    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id');
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class,'cart_id');
    }
}
