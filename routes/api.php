<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

//AUTH
    Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
        Route::post('register/{role}', 'AuthController@Register');
        Route::post('login/{role}', 'AuthController@Login');
        Route::post('logout', 'AuthController@Logout');
        Route::get('profile', 'AuthController@Profile');
        Route::post('edit-profile', 'AuthController@EditProfile');
    });

    Route::resource('settings','SettingsController');



});
